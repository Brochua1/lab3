package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void echoReturns5(){
        int actual = App.echo(5);
        assertEquals("Checks if echo() returns the value it is passed", 5, actual);
    }

    @Test
    public void oneMoreReturns5(){
        int actual = App.oneMore(4);
    }
}
